run:
	make build
	clear
	./calc

build:
	yacc -dv calc.y
	lex calc.l
	clang -o calc y.tab.c lex.yy.c -std=c89


clear:
	rm -rf ./calc ./y.output ./lex.yy.c ./y.tab.h ./y.tab.c
	clear
