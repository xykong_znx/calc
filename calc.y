%{
    #include <stdio.h>
    #include <stdlib.h>
    #define YYDEBUG 1
    extern yyerror ();
    extern yylex ();
    extern yyvsp ();
%}

%union{
    int int_value;
    double double_value;
}

%token <double_value> DOUBLE_LITERAL;
%token ADD SUB MUL DIV CR BL BR;
%type <double_value> expression term primary_expression

%%
line_list
    : line
    | line_list line
    ;
line
    : expression CR
    {
        printf (">>%lf\n", $1);
    }
expression    
    : term
    | expression ADD expression
    {
        $$ = $1 + $3;
    }
    | expression SUB expression
    {
        $$ = $1 - $3;
    }
    | expression MUL expression
    {
        $$ = $1 * $3;
    }
    | expression DIV expression
    {
        $$ = $1 / $3;
    }
    ;
term
    : primary_expression
    | term MUL primary_expression
    {
        $$ = $1 * $3;
    }
    | term DIV primary_expression
    {
        $$ = $1 / $3;
    }
    ;
primary_expression
    : DOUBLE_LITERAL
    | BL expression BR
    {
        $$ = $2;
    }
    | SUB expression
    {
        $$ = 0 - $2;
    }
    ;
%%

int yyerror (const char* str)
{
    extern char *yytext;
    fprintf (stderr, "parser error near %s\n", yytext);
    return 0;
}

int main (void) {
    extern int yyparse (void);
    extern FILE *yyin;

    yyin = stdin;
    if (yyparse()) {
        fprintf (stderr, "Error!\n");
        exit (1);
    }
}
